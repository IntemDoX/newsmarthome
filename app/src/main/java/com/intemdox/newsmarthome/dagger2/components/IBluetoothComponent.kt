package com.intemdox.newsmarthome.dagger2.components

import com.intemdox.newsmarthome.BluetoothHelper
import com.intemdox.newsmarthome.dagger2.modules.ApplicationModule
import dagger.Component

@Component(modules = [ApplicationModule::class])
interface IBluetoothComponent {
  fun getBluetoothHelper(): BluetoothHelper
}