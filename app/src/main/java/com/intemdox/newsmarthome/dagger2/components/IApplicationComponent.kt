package com.intemdox.newsmarthome.dagger2.components

import android.app.Application
import android.content.Context
import com.intemdox.newsmarthome.BluetoothHelper
import com.intemdox.newsmarthome.dagger2.modules.ApplicationModule
import com.intemdox.newsmarthome.viewmodels.LedConnectionViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    val context: Context

    val application: Application

    val bluetoothHelper: BluetoothHelper

    fun inject(demoApplication: LedConnectionViewModel)

}