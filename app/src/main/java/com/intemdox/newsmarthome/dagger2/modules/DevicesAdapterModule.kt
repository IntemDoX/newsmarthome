package com.intemdox.newsmarthome.dagger2.modules

import com.intemdox.newsmarthome.entity.BTDeviceEntity
import dagger.Module
import dagger.Provides

@Module
class DevicesAdapterModule(private val devicesList: List<BTDeviceEntity>) {
  @Provides
  fun provideDevicesList(): List<BTDeviceEntity> {
    return devicesList
  }
}