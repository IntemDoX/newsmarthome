package com.intemdox.newsmarthome.dagger2.components

import com.intemdox.newsmarthome.DevicesAdapter
import com.intemdox.newsmarthome.dagger2.modules.DevicesAdapterModule
import dagger.Component

@Component(modules = [(DevicesAdapterModule::class)])
interface IAdapterComponent {
    fun getDevicesAdapter(): DevicesAdapter
}