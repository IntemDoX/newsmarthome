package com.intemdox.newsmarthome.dagger2.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(val application: Application) {
  @Provides
  fun provideApp(): Application {
    return application
  }

  @Provides

  fun provideContext(): Context {
    return application
  }
}