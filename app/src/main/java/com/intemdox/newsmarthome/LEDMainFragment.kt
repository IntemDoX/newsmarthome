package com.intemdox.newsmarthome

import android.arch.lifecycle.ViewModelProviders
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.graphics.ColorUtils
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.intemdox.newsmarthome.entity.ColorEntity
import com.intemdox.newsmarthome.viewmodels.LEDMainViewModel
import com.madrapps.pikolo.listeners.SimpleColorSelectionListener
import kotlinx.android.synthetic.main.fragment_led_main.*
import java.util.*

class LEDMainFragment : Fragment(), View.OnClickListener {
    var mainColor: Int = 0
    private lateinit var ledViewModel: LEDMainViewModel
    val random = Random()

    companion object {
        const val COLOR_TYPE_RECENT = 1
        const val COLOR_TYPE_SAVED = 0
        @JvmStatic
        fun newInstance() =
                LEDMainFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ledViewModel = ViewModelProviders.of(this, LEDMainViewModel.Factory(activity!!.application)).get(LEDMainViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_led_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_save_color.setOnClickListener(this)
        btn_accept_color.setOnClickListener(this)
        randomButton.setOnClickListener(this)
        btn_mode.setOnClickListener(this)
        colorPicker.setColorSelectionListener(object : SimpleColorSelectionListener() {
            override fun onColorSelected(color: Int) {
                imageView.background.setColorFilter(color, PorterDuff.Mode.MULTIPLY)
                mainColor = color
            }
        })
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_mode -> {
                showAlert()
            }
            btn_save_color -> {
                btn_save_color.isEnabled = false
                val c = ColorEntity(mainColor, 0)
                ledViewModel.saveColor(c)
                Handler().postDelayed({
                    btn_save_color.isEnabled = true
                }, 1000)
            }
            btn_accept_color -> {
                btn_accept_color.isEnabled = false
                val c = ColorEntity(mainColor, 1)
                ledViewModel.acceptColor(c, true)
                Handler().postDelayed({
                    btn_accept_color.isEnabled = true
                }, 1000)
            }
            randomButton -> {
                val color = ColorUtils.HSLToColor(floatArrayOf(random.nextInt(360).toFloat(), random.nextFloat(), random.nextFloat()))
                val hexColor = String.format("#%06X", 0xFFFFFF and color)
                randomButton.text = hexColor
                imageView.background.setColorFilter(color, PorterDuff.Mode.MULTIPLY)
                colorPicker.setColor(color)
                ledViewModel.acceptColor(ColorEntity(mainColor, 1), true)
            }
        }
    }

    private fun showAlert() {
        val dialog = AlertDialog.Builder(context!!).setItems(context!!.resources.getStringArray(R.array.modes)
        ) { _, which ->
            ledViewModel.saveMode(which)
        }
        dialog.show()
    }
}