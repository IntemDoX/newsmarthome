package com.intemdox.newsmarthome

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.intemdox.newsmarthome.entity.BTDeviceEntity
import kotlinx.android.synthetic.main.item_btdevice.view.*
import javax.inject.Inject

class DevicesAdapter @Inject constructor(private val devicesList: List<BTDeviceEntity>) : RecyclerView.Adapter<DevicesAdapter.ViewHolder>() {

    lateinit var onItemClickListener: OnItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_btdevice, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return devicesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.deviceName.text = devicesList[position].name
        holder.container.setOnClickListener { onItemClickListener.connectToDevice(devicesList[position].macAddress) }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val deviceName = view.device_name_value!!
        val container = view.container!!
    }

    interface OnItemClickListener {
        fun connectToDevice(macAddress: String)
    }
}