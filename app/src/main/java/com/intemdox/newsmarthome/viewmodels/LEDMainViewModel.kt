package com.intemdox.newsmarthome.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.SharedPreferences
import android.graphics.Color
import android.support.annotation.NonNull
import android.util.Log
import com.intemdox.newsmarthome.BluetoothHelper
import com.intemdox.newsmarthome.dagger2.components.DaggerIBluetoothComponent
import com.intemdox.newsmarthome.dagger2.modules.ApplicationModule
import com.intemdox.newsmarthome.entity.ColorEntity

class LEDMainViewModel(application: Application) : AndroidViewModel(application), BluetoothHelper.IBluetoothCommandCallback {
    companion object {
        const val MODE_SINGLE_COLOR = 0
        const val MODE_PULSE_ONE_COLOR = 1
    }
    private var mode = 0
    override fun onCommandSuccess() {
        Log.d("testtest","COMMAND SENT SUCCESS!!!")
    }

    override fun onError(throwable: Throwable) {
        Log.e("testtest", "error ", throwable)
    }

    var btHelper: BluetoothHelper

    init {
        val btComponent = DaggerIBluetoothComponent.builder().applicationModule(ApplicationModule(application)).build()
        btHelper = btComponent.getBluetoothHelper()
    }

    fun saveColor(colorEntity: ColorEntity) {
        Log.d("testtest", "Save color...")
    }

    fun saveMode(mode: Int) {
        this.mode = mode
        //todo save to shared
    }

    fun acceptColor(colorEntity: ColorEntity, isSaveColor: Boolean) {
        val r = Color.red(colorEntity.color)
        val g = Color.green(colorEntity.color)
        val b = Color.blue(colorEntity.color)
        btHelper.sendCommand(this, intArrayOf(mode, r, g, b))
        if (isSaveColor) {
            saveColor(colorEntity)
        }
    }

    class Factory(@NonNull var application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LEDMainViewModel(application) as T
        }

    }

}