package com.intemdox.newsmarthome.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.intemdox.newsmarthome.BluetoothHelper
import com.intemdox.newsmarthome.dagger2.components.DaggerIBluetoothComponent
import com.intemdox.newsmarthome.dagger2.modules.ApplicationModule
import com.intemdox.smarthome.exception.BTNotEnableException
import com.intemdox.smarthome.exception.BTNotExistException
import io.reactivex.annotations.NonNull
import kotlinx.android.synthetic.main.fragment_led_connection.*

class LedConnectionViewModel(application: Application) : AndroidViewModel(application) {
    private var btHelper: BluetoothHelper

    init {
        val btComponent = DaggerIBluetoothComponent.builder().applicationModule(ApplicationModule(application)).build()
        btHelper = btComponent.getBluetoothHelper()

    }

    fun connectToDevice(btCallbackListener: BluetoothHelper.IBluetoothConnectionCallback, address: String) {
        btHelper.connectToDevice(btCallbackListener, address)
    }

    fun getBluetoothPairedDevices(isAdapterExist: Boolean, btCallbackListener: BluetoothHelper.IBluetoothDevicesCallback) {
        btHelper.getPairedDeviceList(btCallbackListener)
    }


    fun isAdapterEnable(isAdapterExist: Boolean): Boolean {
        return btHelper.isAdapterEnable() && isAdapterExist
    }

    class Factory(application: Application): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LedConnectionViewModel(application) as T
        }

        @NonNull
        var application : Application = application

    }
}