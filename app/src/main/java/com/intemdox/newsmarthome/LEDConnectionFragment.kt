package com.intemdox.newsmarthome

import android.arch.lifecycle.ViewModelProviders
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.navigation.Navigation
import com.intemdox.newsmarthome.dagger2.components.DaggerIAdapterComponent
import com.intemdox.newsmarthome.dagger2.modules.DevicesAdapterModule
import com.intemdox.newsmarthome.entity.BTDeviceEntity
import com.intemdox.newsmarthome.viewmodels.LedConnectionViewModel
import com.intemdox.smarthome.exception.BTNotEnableException
import com.intemdox.smarthome.exception.BTNotExistException
import kotlinx.android.synthetic.main.fragment_led_connection.*
import java.util.*

class LEDConnectionFragment : Fragment(), BluetoothHelper.IBluetoothEnableCallback,
        BluetoothHelper.IBluetoothConnectionCallback, BluetoothHelper.IBluetoothDevicesCallback,
        DevicesAdapter.OnItemClickListener, View.OnClickListener {
    private val TAG: String = "LedConnectionFragment"
    private lateinit var ledConnectionViewModel: LedConnectionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ledConnectionViewModel = ViewModelProviders.of(this, LedConnectionViewModel.Factory(activity!!.application)).get(LedConnectionViewModel::class.java)
    }


    private fun initAdapter(devicesList: List<BTDeviceEntity>) {
        if (devicesList.isEmpty()) {
            showMessage(getString(R.string.bt_no_devices_found_message))
            return
        }
        showMessage(isVisible = false)
        devices_list.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        val deviceAdapterComponent = DaggerIAdapterComponent.builder().devicesAdapterModule(DevicesAdapterModule(devicesList)).build()
        val adapter = deviceAdapterComponent.getDevicesAdapter()
        adapter.onItemClickListener = this
        devices_list.adapter = adapter
        if (devicesList.isNotEmpty()) {
            devices_list.visibility = View.VISIBLE
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_find_devices -> {
                ledConnectionViewModel.getBluetoothPairedDevices(devices_list.adapter != null,this)
            }
            btn_connect_available_devices -> {
              Log.d("testtest", "Nothing to do!!!")
            }
        }
    }


    private fun showMessage(text: String = "", isVisible: Boolean = true) {
        if (isVisible) {
            message.visibility = View.VISIBLE
            message.text = text
        } else {
            message.visibility = View.GONE
        }
    }

    override fun connectToDevice(macAddress: String) {
        ledConnectionViewModel.connectToDevice(this, macAddress)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_led_connection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_find_devices.background.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY)
        btn_find_devices.setOnClickListener(this)
    }

    override fun onError(exception: Throwable) {
        Log.e("testtest", "error ", exception)
        when (exception) {
            is BTNotExistException -> showMessage(getString(R.string.bt_not_supported_message))
            is BTNotEnableException -> {
                val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBluetoothIntent, MainActivity.REQUEST_ENABLE_BLUETOOTH)
            }
        }
    }

    override fun onSuccess(list: ArrayList<BTDeviceEntity>) {
        initAdapter(list)
        Log.d("testtest", "Devices found. Count = " + list.size)
    }


    override fun onConnectionSuccess() {
        Log.d("testtest", "Connection success")
        val navController = Navigation.findNavController(view!!)
        navController.navigate(R.id.ledMainFragment)
    }

    override fun onConnectionFailed(throwable: Throwable) {
        Log.d("testtest", "Connection failed :(")
    }

    override fun onSuccess() {
        Log.d("testtest", "Bluetooth is enabled")
    }
}