package com.intemdox.newsmarthome.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "btdevices_table")
class BTDeviceEntity {
  @PrimaryKey
  @NonNull
  @ColumnInfo(name = "mac_address")
  lateinit var macAddress: String

  @NonNull
  @ColumnInfo(name = "name")
  lateinit var name: String

  @ColumnInfo(name = "device_type")
  lateinit var deviceType: String
}