package com.intemdox.newsmarthome

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.util.Log
import com.intemdox.newsmarthome.entity.BTDeviceEntity
import com.intemdox.smarthome.exception.BTNotEnableException
import com.intemdox.smarthome.exception.BTNullSocketException
import java.io.IOException
import java.util.*
import javax.inject.Inject


class BluetoothHelper @Inject constructor() {
    companion object {
        const val TAG = "testtest_btHelper"
        const val EXTRA_ADDRESS: String = "Device_address"
        var myUUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        var isConnected: Boolean = false
        var btSocket: BluetoothSocket? = null
    }

    private var btAdapter: BluetoothAdapter? = null
    private lateinit var pairedDevices: Set<BluetoothDevice>
    private lateinit var address: String
    private val KEY_PREF = "shared"
    private val KEY_MAC = "mac"

    fun sendCommand(bt: IBluetoothCommandCallback, input: IntArray) {
        if (btSocket != null) {
            try {
                for (i in input) {
                    btSocket!!.outputStream.write(i)
                }
                bt.onCommandSuccess()
                Log.d(TAG, "sendCommand success")
            } catch (e: IOException) {
                Log.d(TAG, "sendCommand error " + e.message)
                bt.onError(e)
            }
        } else {
            try {
                val device: BluetoothDevice? = btAdapter?.getRemoteDevice(address)
                btSocket = device?.createInsecureRfcommSocketToServiceRecord(myUUID)
                for (i in input) {
                    btSocket!!.outputStream.write(i)
                }
                bt.onCommandSuccess()
            } catch (e: IOException) {
                Log.d(TAG, "sendCommand error " + e.message)
                bt.onError(BTNullSocketException())
            }
        }
    }

    fun connectToDevice(btCallbackListener: IBluetoothConnectionCallback, address: String) {
        this.address = address
        try {
            if (btSocket == null || !isConnected) {
                btAdapter = BluetoothAdapter.getDefaultAdapter()
                if (btAdapter == null) {
                    btCallbackListener.onConnectionFailed(BTNotEnableException())
                } else {
                    val device: BluetoothDevice? = btAdapter?.getRemoteDevice(address)
                    btSocket = device?.createInsecureRfcommSocketToServiceRecord(myUUID)
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
                    btSocket!!.connect()
                    btCallbackListener.onConnectionSuccess()
                }
            } else {
                btSocket!!.connect()
                btCallbackListener.onConnectionSuccess()
            }
        } catch (e: IOException) {
            btCallbackListener.onConnectionFailed(e)
        }
    }

    fun enableBT() {
        btAdapter = BluetoothAdapter.getDefaultAdapter()
    }

    fun getPairedDeviceList(btCallbackListener: IBluetoothDevicesCallback) {
        if (btAdapter == null) {
            enableBT()
        }
        pairedDevices = btAdapter!!.bondedDevices
        val list: ArrayList<BluetoothDevice> = ArrayList()
        if (!pairedDevices.isEmpty()) {
            for (device: BluetoothDevice in pairedDevices) {
                list.add(device)
                Log.d(TAG, "add device: ${device.name}")
            }
        }
        btCallbackListener.onSuccess(convertToDeviceList(list))
    }

    fun convertToDeviceList(list: ArrayList<BluetoothDevice>): ArrayList<BTDeviceEntity> {
        val entityList: ArrayList<BTDeviceEntity> = ArrayList()
        for (bluetoothDevice: BluetoothDevice in list) {
            val value = BTDeviceEntity()
            value.name = bluetoothDevice.name
            value.macAddress = bluetoothDevice.address
            entityList.add(value)
        }
        return entityList
    }

    fun isBluetoothExist(): Boolean {
        return btAdapter != null
    }


    fun isAdapterEnable(): Boolean {
        val isAdapterEnabled = btAdapter?.isEnabled
        return isAdapterEnabled != null && isAdapterEnabled
    }

    interface IBluetoothEnableCallback {
        fun onSuccess()
        fun onError(throwable: Throwable)
    }

    interface IBluetoothConnectionCallback {
        fun onConnectionSuccess()
        fun onConnectionFailed(throwable: Throwable)
    }

    interface IBluetoothDevicesCallback {
        fun onSuccess(list: ArrayList<BTDeviceEntity>)
        fun onError(throwable: Throwable)
    }

    interface IBluetoothCommandCallback {
        fun onCommandSuccess()
        fun onError(throwable: Throwable)
    }
}